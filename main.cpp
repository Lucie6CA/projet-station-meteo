/*
 * Copyright (c) 2018, CATIE
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "mbed.h"
#include <nsapi_dns.h>
#include <MQTTClientMbedOs.h>
#include "bma280.h"
#include "bme280.h"

namespace {
#define MQTT_TOPIC_TEMPERATURE     "Lucie6/feeds/temperature"
#define MQTT_TOPIC_HUMIDITY     "Lucie6/feeds/humidity"
#define MQTT_TOPIC_PRESSURE     "Lucie6/feeds/pressure"
#define MQTT_TOPIC_LED     "Lucie6/feeds/led"
#define SYNC_INTERVAL           1
#define PERIOD_MS 1000

}

using namespace sixtron;

// Peripherals
static DigitalOut led(LED1);
static InterruptIn button(BUTTON1);
I2C i2c(I2C_SDA, I2C_SCL);

BMA280 bma(&i2c);
BME280 bme(&i2c);

// Network 
NetworkInterface *network;
MQTTClient *client;

// MQTT
const char* hostname = "io.adafruit.com";
int port = 1883;

// Error code
nsapi_size_or_error_t rc = 0;

// Event queue
static int id_yield;
EventQueue *main_queue = mbed_event_queue();

static int id_sendth;

/*!
*  \brief Called when a message is received
*
*  Print messages received on mqtt topic
*/
void messageArrived(MQTT::MessageData& md)
{
	MQTT::Message &message = md.message;
    printf("Message arrived: qos %d, retained %d, dup %d, packetid %d\r\n", message.qos, message.retained, message.dup, message.id);
    printf("Payload %.*s\r\n", message.payloadlen, (char*)message.payload);
    

    // Get the payload string
	char* char_payload = (char*)malloc((message.payloadlen+1)*sizeof(char));
	char_payload = (char *) message.payload;
	char_payload[message.payloadlen] = '\0'; // String must be null terminated

	if (strcmp(char_payload, "ON") == 0) {
		led = 1;
	}
	else if (strcmp(char_payload, "OFF") == 0) {
		led = 0;
	}
	else if (strcmp(char_payload, "RESET") == 0) {
		printf("RESETTING ...\n");
		system_reset();
	}
}

/*!
*  \brief Yield to the MQTT client
*
*  On error, stop publishing and yielding
*/
static void yield(){
    rc = client->yield(100);

    if (rc != 0){
        printf("Yield error: %d\n", rc);
        main_queue->cancel(id_yield);
        main_queue->break_dispatch();
        system_reset();
    }
}
/*!
*  \brief Publish data over the corresponding adafruit MQTT topic
*
*/


void sendth(){
	// Temperature
	char mqttPayload[20];
	float temperature = bma.temperature(); // le float à convertir en string
	sprintf(mqttPayload, "%.3f", temperature);

	MQTT::Message message;
	message.qos = MQTT::QOS1;
	message.retained = false;
	message.dup = false;
	message.payload = (void*)mqttPayload;
	message.payloadlen = strlen(mqttPayload);

	printf("Send: %s to MQTT Broker: %s\n", mqttPayload, hostname);
	rc = client->publish(MQTT_TOPIC_TEMPERATURE, message);

	// Humidity
	float humidity = bme.humidity();
	sprintf(mqttPayload,"%.3f", bme.humidity());

	message.qos = MQTT::QOS1;
	message.retained = false;
	message.dup = false;
	message.payload = (void*)mqttPayload;
	message.payloadlen = strlen(mqttPayload);

	printf("Send: %s to MQTT Broker: %s\n", mqttPayload, hostname);
	rc = client->publish(MQTT_TOPIC_HUMIDITY, message);
}

static int8_t publish() {


	char mqttPayload[20]; // Création du tableau de caractère
	float pressure = (bme.pressure() / 100.0f); // le float à convertir en string
	sprintf(mqttPayload, "%.3f", pressure);
	MQTT::Message message;
	message.qos = MQTT::QOS1;
	message.retained = false;
	message.dup = false;
	message.payload = (void*)mqttPayload;
	message.payloadlen = strlen(mqttPayload);
	printf("Send: %s to MQTT Broker: %s\n", mqttPayload, hostname);

	rc = client->publish(MQTT_TOPIC_PRESSURE, message);
	if (rc != 0) {
		printf("Failed to publish: %d\n", rc);
		return rc;
	}

	return 0;
}

// main() runs in its own thread in the OS
// (note the calls to ThisThread::sleep_for below for delays)
int main()
{
	printf("Connecting to border router...\n");

    /* Get Network configuration */
	network = NetworkInterface::get_default_instance();

	if (!network) {
		printf("Error! No network interface found.\n");
		return 0;
	}

	/* Add DNS */
	nsapi_addr_t new_dns = {
		NSAPI_IPv6,
		{ 0xfd, 0x9f, 0x59, 0x0a, 0xb1, 0x58, 0, 0, 0, 0, 0, 0, 0, 0, 0x00, 0x01 }
	};
	nsapi_dns_add_server(new_dns, "LOWPAN");

	/* Border Router connection */
	rc = network->connect();
	if (rc != 0) {
		printf("Error! net->connect() returned: %d\n", rc);
		return rc;
	}

	/* Print IP address */
	SocketAddress a;
	network->get_ip_address(&a);
	printf("IP address: %s\n", a.get_ip_address() ? a.get_ip_address() : "None");

	/* Open TCP Socket */
	TCPSocket socket;
	/* MQTT Connection */
	client = new MQTTClient(&socket);
	socket.open(network);
	rc = socket.connect(hostname, port);
	if(rc != 0){
		printf("Connection to MQTT broker Failed");
		return rc;
	}
	MQTTPacket_connectData data = MQTTPacket_connectData_initializer;
	data.MQTTVersion = 4;
	data.keepAliveInterval = 25;
	data.clientID.cstring = "lola";
	data.username.cstring = (char*) "Lucie6"; // Adafruit username
	data.password.cstring = (char*) "aio_SlrE65y7g7VRSkyDn53TWa4Cg15q"; // Adafruit user key
	if (client->connect(data) != 0){
		printf("Connection to MQTT Broker Failed\n");
	}
	/* MQTT Subscribe */
	if ((rc = client->subscribe(MQTT_TOPIC_LED, MQTT::QOS0, messageArrived)) != 0){
			   printf("rc from MQTT subscribe is %d\r\n", rc);
	}

	bma.initialize();
	bme.initialize();
	bme.set_sampling();




	yield();

	// Yield every 1 second
	id_yield = main_queue->call_every(SYNC_INTERVAL * 1000, yield);

	// Publish
	//toutes les 2 secondes
	sendth();
	id_sendth=main_queue->call_every(3 * 1000, sendth);
	//quand on appuie sur le bouton
	button.fall(main_queue->event(publish));


	main_queue->dispatch_forever();



}

/*ERREUR AU BOUT DE 3 VALEURS
 * ++ MbedOS Error Info ++
Error Status: 0x80FF0144 Code: 324 Module: 255
Error Message: Assertion failed: _readers == 0
Location: 0x8032449
File: ./mbed-os/features/netsocket/TCPSocket.cpp+209
Error Value: 0x0
Current Thread: main Id: 0x2000D390 Entry: 0x803334F StackSize: 0x1000 StackMem: 0x2000DEB0 SP: 0x2000EBBC
For more info, visit: https://mbed.com/s/error?error=0x80FF0144&tgt=ZEST_CORE_STM32L4A6RG
-- MbedOS Error Info --
 */







